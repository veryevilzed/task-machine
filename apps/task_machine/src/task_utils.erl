-module(task_utils).

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
-endif.

-export([get_values/3, get_value/3]).


get_value([{K, V}|_], Key, _) when K =:= Key ->
	V;
get_value([{K, _V}|Args], Key, Default) when K =/= Key ->
	get_value(Args, Key, Default);
get_value([], _, Default) ->
	Default.


get_values(Args, [Key|Keys], [Default|Defaults], Data) ->
	get_values(Args, Keys, Defaults, Data ++ [ get_value(Args, Key, Default) ]);
get_values(_Args, [], [], Data) ->
	Data.


get_values(Args, Keys, Defaults) ->
	get_values(Args, Keys, Defaults, []).




-ifdef(EUNIT).

get_value_test() ->
	[
		?assert(get_value([{bet,1},{line,9}], bet, 100) =:= 1),
		?assert(get_value([{bet,1},{line,9}], kf, 100) =:= 100),
		?assert(get_value([{bet,1},{line,9}], line, 1) =:= 9),
		?assert([get_value([{bet,1},{line,9}], line, 1)|[]] =:= [9])
	].

get_values_test() ->
	?assert(get_values([{bet,1},{line,9}],[],[]) =:= []),
	?assert(get_values([{bet,1},{line,9}],[bet,line,kf],[999,999,100]) =:= [1,9,100]).


-endif.