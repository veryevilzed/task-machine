-module(task_machine_db).

-include("task_machine_db.hrl").
-include_lib("stdlib/include/qlc.hrl").

-define(PURGE_TIMEOUT, 5).
-define(PURGE_NOT_DONE_TIMEOUT, 120).

-export([start/0, install/1, change_task_status/3, get_task/1, take_task/0, add_task/2, purge/0, get_all_task/0, create_tables/1]).

start() ->
	application:start(mnesia),
	mnesia:wait_for_tables([task], 2000),
	ok.


transaction(F) ->
	mnesia:activity(transaction, F).


%% Создание таблиц
create_tables(Nodes) ->
	case mnesia:create_table(task,
				[{attributes, record_info(fields, task)},
				{index, []},
				{storage_properties,[ 	
					{ets, [compressed]}, 
					{dets, [{auto_save, 3000}]} 
				]}, {disc_copies, Nodes}]) of
		{aborted, {already_exists, TableName}} -> lager:log(info, self(), "Table ~p Already Exist", [TableName]);
		{atomic,ok} -> lager:log(info, self(), "Table Created");
		{aborted, Reason} -> lager:log(error, self(), "Create table Error ~p", [Reason])
	end,
	ok.


install(Nodes) ->
	case  mnesia:create_schema(Nodes) of 
		ok ->
			application:start(mnesia),
			case mnesia:create_table(task,
						[{attributes, record_info(fields, task)},
						{index, []},
						{storage_properties,[ 	
							{ets, [compressed]}, 
							{dets, [{auto_save, 3000}]} 
						]}, {disc_copies, Nodes}]) of
				{aborted, {already_exists, TableName}} -> lager:log(info, self(), "Table ~p Already Exist", [TableName]);
				{atomic,ok} -> lager:log(info, self(), "Table Created");
				{aborted, Reason} -> lager:log(error, self(), "Create table Error ~p", [Reason])
			end,
			application:stop(mnesia);
		Error -> lager:log(info, self(), "Create schema error ~p", [Error])
	end,
	ok.


date_diff(Date) -> %% В Минутах
	{Day, Time} = calendar:time_difference(calendar:local_time(), Date),
	erlang:abs(Day * 24 * 60  + (calendar:time_to_seconds(Time)/60)).

add_task(TaskName, Args) ->
	transaction(fun() ->
		UUID=uuid:to_string(uuid:uuid1()),
		mnesia:write(#task{
				uuid=UUID,
				taskname=TaskName,
				args=Args,
				date=calendar:local_time()}),
		{ok, UUID}
	end).
	




change_task_status(UUID, done, Result) ->
	change_task_status_to(UUID, done, true, Result);
change_task_status(UUID, work, _) ->
	change_task_status_to(UUID, work, false, "");
change_task_status(UUID, error, ErrorResult) ->
	change_task_status_to(UUID, error, true, ErrorResult);
change_task_status(_, _, _) ->
	{error, status_error}.



change_task_status_to(UUID, Status, Done, Result) ->
	transaction(fun() ->
		Query = qlc:q([ Task || Task <- mnesia:table(task), Task#task.done =:= false, Task#task.uuid =:= UUID ]), 
		case qlc:eval(Query) of
			[R] -> 
				NewR = R#task{status=Status, done=Done, result=Result},
				mnesia:write(NewR),
				{ok, NewR};
			[_|_] -> 
				{error, uuid_hell};
			[] -> 
				{empty}
		end
	end).

purge() ->
	R = transaction(fun() ->
		Query = qlc:q([ Task || Task <- mnesia:table(task), Task#task.done =:= true, date_diff(Task#task.date) > ?PURGE_TIMEOUT]),
		qlc:eval(Query)
	end),
	purge(R),
	R2 = transaction(fun() ->
		Query = qlc:q([ Task || Task <- mnesia:table(task), Task#task.done =:= false, date_diff(Task#task.date) > ?PURGE_NOT_DONE_TIMEOUT]),
		qlc:eval(Query)
	end),
	purge(R2).



purge(Tasks) ->
	transaction(fun() ->
		[mnesia:delete_object(Task) || Task <- Tasks]
	end).



get_task(UUID) ->
	transaction(fun() ->
		%% Не стер просто пробовал другой вариант
		%%Query = qlc:q([ Task || Task <- mnesia:table(task), Task#task.uuid =:= UUID ]), 
		%%case qlc:eval(Query) of
		case mnesia:dirty_read({task, UUID}) of
			[R] ->
				{ok, R};
			[_|_] ->
				{error, uuid_hell};
			[] ->
				{empty}
		end
	end).


get_all_task() ->
	transaction(fun() ->
		Query = qlc:q([ Task || Task <- mnesia:table(task)]),
		qlc:eval(Query)
	end).

take_task() ->
	transaction(fun() ->
		Query = qlc:q([ Task || Task <- mnesia:table(task), Task#task.done =:= false, Task#task.status =:= queue ]), 
		case qlc:eval(Query) of
			[R|_] -> 
				NewR = R#task{status=worked},
				mnesia:write(NewR),
				{ok, NewR};
			[] -> 
				{empty}
		end
	end).
