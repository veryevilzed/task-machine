-module(task_machine_handlers).
-include("task_machine_db.hrl").
-export([init/3, handle/2, terminate/3]).

-export([list/3, add_task/3, get_task/3]).

-define(CT_JSON, {<<"content-type">>, <<"application/json">>}).
-define(bta(Data), binary_to_atom(Data, utf8)).

init(_Transport, Req, []) -> {ok, Req, undefined}.

handle(Req, State) -> {ok, dispatch(Req), State}.

dispatch(Req) ->
	{Method, Req2}    = cowboy_req:method(Req),
    {[Fn|Data], Req3} = cowboy_req:path_info(Req2),
    try
    	{Result, Req4} = apply(?MODULE, ?bta(Fn), [?bta(Method), Data, Req3]),
    	json(Result, Req4)
    catch
    	_:Reason -> task_error({Reason, erlang:get_stacktrace()}, Req3) %{error, {Reason, erlang:get_stacktrace()} }
    end.
    


json(Data, Req) ->
	lager:log(info, self(), "-TO JSON-> ~p", [Data]),
    {ok, Reply} = cowboy_req:reply(200, [?CT_JSON], jsx:encode(Data), Req), 
    Reply.


list('GET', [], Req) ->
	{[ [ 
			{<<"uuid">>, erlang:list_to_binary(Task#task.uuid) }, 
			{<<"status">>, erlang:atom_to_binary(Task#task.status, utf8)},
			{<<"done">>, Task#task.done},
			{<<"args">>, erlang:list_to_binary( io_lib:format("~p", [Task#task.args])) },
			{<<"result">>, erlang:list_to_binary( io_lib:format("~p", [Task#task.result])) } 
		] || Task <- task_machine_db:get_all_task()], 
	Req}.


get_task('GET', [TaskName], Req) ->
	{UUID, Req2} = cowboy_req:qs_val(<<"uuid">>, Req, ""), 
	case task_machine_api:get_task(erlang:binary_to_list(UUID)) of
		{ok, Result, lifetime, L} ->
			Res = case Result of
				{json, Data} -> {<<"result">>, Data };
				_ -> {<<"result">>, erlang:list_to_binary( io_lib:format("~p", [Result])) } 
			end,
			{[ 
				{<<"uuid">>, UUID }, 
				{<<"status">>, <<"done">>},
				{<<"done">>, true},
				{<<"lifetime">>, L},
				Res
			], Req2};
		{error, Res} ->
			{[ 
				{<<"uuid">>, UUID }, 
				{<<"status">>, <<"error">> },
				{<<"done">>, true},
				{<<"result">>, erlang:list_to_binary( io_lib:format("~p", [Res])) } 
			], Req2};
		Status ->
			{[ 
				{<<"uuid">>, UUID }, 
				{<<"status">>, <<"uncnown">> },
				{<<"result">>, erlang:list_to_binary( io_lib:format("~p", [Status])) }
			], Req2}
	end;
get_task('GET', [], Req) ->
	get_task('GET', [task], Req).

add_task('GET', [TaskName], Req) ->
	{KV, Req2} = cowboy_req:qs_vals(Req),
	{ok, UUID} = task_machine_api:add_task(erlang:binary_to_atom(TaskName, utf8), KV),
	{ [{<<"uuid">>, erlang:list_to_binary(UUID) }], Req2};
add_task('GET', [], Req) ->
	add_task('GET', [task], Req).

get_result('GET', [result, UUID], Req) ->
	{[], Req}.


task_error(Reason, Req) ->
	{ok, Reply} = cowboy_req:reply(200, [{<<"content-type">>, <<"text/plain">>}], erlang:list_to_binary( io_lib:format("500 Error: ~p", [Reason])) , Req),
	Reply.
    


terminate(_Reason, _Req, _State) ->
	ok.