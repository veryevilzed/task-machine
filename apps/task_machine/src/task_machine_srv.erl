-module(task_machine_srv).
-behaviour(gen_server).

-include("task_machine_db.hrl").

-define(REFRESH_INTERVAL, 3000).
-define(TASK_MODULE, task_machine_task).

-export([start_link/0, stop/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, code_change/3, terminate/2]).

-record(state, {refs=gb_sets:empty(), max_count=5}).

start_link() ->
	lager:log(info, self(), "Start Server"),
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

stop() ->
	gen_server:call({local, ?MODULE}, stop).
 

init([]) -> 
	erlang:send_after(100, self(), refresh),
	{ok, []}.


handle_call(stop, _From, State) -> {stop, normal, ok, State};
handle_call({add_task, TaskName, Args}, _From, State) ->  {reply, task_machine_db:add_task(TaskName, Args), State};
handle_call(_Request, _From, State) -> {reply, ok, State}.
	

handle_cast({add_task, TaskName, Args}, State) -> task_machine_db:add_task(TaskName, Args),  {noreply, State};
handle_cast(_Msg, State) -> {noreply, State}.



execute_tesk(TaskName, Args) ->
	try
		{done, erlang:apply(?TASK_MODULE, TaskName, [Args]) }
	catch
		_:Reason -> 
			lager:log(error, self(), "Execute task Error: ~p / ~p", [Reason, erlang:get_stacktrace()]),
			{error, {Reason, erlang:get_stacktrace()} }
	end.


handle_info(_Info, []) ->
	handle_info(_Info, #state{});

handle_info(refresh, State=#state{max_count=N, refs=R}) when N > 0 -> 
	%% Получить сообщение из очереди и отправить его на выполнение
	task_machine_db:purge(),
	case task_machine_db:take_task() of
		{ok, #task{uuid=UUID, taskname=TaskName, args=Args} } ->
			Pid = spawn(fun() ->  
				{Status, Result}  = execute_tesk(TaskName, Args),
				task_machine_db:change_task_status(UUID, Status, Result) 
			end),
			Refs = erlang:monitor(process, Pid),
			erlang:send_after(?REFRESH_INTERVAL, self(), refresh),
			{noreply, State#state{refs=gb_sets:add(Refs, R), max_count=N-1} };
		{empty} ->
			erlang:send_after(?REFRESH_INTERVAL, self(), refresh),
			{noreply, State}
	end;

handle_info(refresh, State=#state{max_count=N}) when N =< 0 ->
	erlang:send_after(?REFRESH_INTERVAL, self(), refresh),
	{noreply, State};


handle_info({'DOWN', Ref, process, _Pid, _Reasone}, State = #state{refs=R, max_count=N}) -> %% Процесс завершился
	case gb_sets:is_element(Ref, R) of
		true ->
			{noreply, State#state{refs=gb_sets:delete(Ref,R), max_count=N+1} };
		false ->
			{noreply, State}
	end;
handle_info(_Info, State) -> {noreply, State}.


terminate(_Reason, _State) -> ok.

code_change(_OldVsn, State, _Extra) -> {ok, State}.
