-module(task_machine_not_found_handlers).


-export([init/3, handle/2, terminate/3]).


init(_Transport, Req, []) ->
	{ok, Req, undefined}.

handle(Req, State) ->
	{ok, Req2} = cowboy_req:reply(404, [{<<"content-type">>, <<"text/plain">>}], "404 NotFound", Req),
	{ok, Req2, State}.

terminate(_Reason, _Req, _State) ->
	ok.