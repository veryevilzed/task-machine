-module(task_machine_task).

-export([task/1, task_generate_spin/1]).

-define(PATH_TO_C, "/Users/zed/src/mobcas/mobcas-cpp/").

task(Args) ->
	timer:sleep(2000), 
	{ok, "Done", ""}.
	


task_generate_spin(Args) ->
	lager:log(info, self(), "Request task_generate_spin ~p", [Args]), 
	Params = [
		?PATH_TO_C,
		task_utils:get_value(Args, name, "dolphins_and_pearls"),
		" -b=" ++ task_utils:get_value(Args, bet, "1"), 
		" -l=" ++ task_utils:get_value(Args, line, "1"), 
		" -m=" ++ task_utils:get_value(Args, m, "0"), 
		" -n=" ++ task_utils:get_value(Args, n, "0"), 
		" -x=" ++ task_utils:get_value(Args, x, "1"), 
		" -t=" ++ task_utils:get_value(Args, t, "r"),
		case task_utils:get_value(Args, e, 0) of
			1 -> " -e";
			_ -> ""
		end,
		" -c=" ++ task_utils:get_value(Args, c, "1")
	],
	lager:log(info, self(), "TaskParams:~p", [lists:flatten(Params)]),
	Result = erlang:list_to_binary(os:cmd(Params)),
	lager:log(info, self(), "TaskResult:~p", [Result]),
	Response = jsx:decode(Result),
	{json, Response}.
