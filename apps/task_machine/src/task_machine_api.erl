-module(task_machine_api).
-include("task_machine_db.hrl").

-define(SRV, task_machine_srv).
-export([add_task/1, add_task/2, get_task/1]).


date_diff(Date) -> %% В Минутах
	{Day, Time} = calendar:time_difference(calendar:local_time(), Date),
	erlang:abs(Day * 24 * 60  + (calendar:time_to_seconds(Time)/60)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Helpers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

add_task(TaskName, Args) ->
	gen_server:call(?SRV, {add_task, TaskName,  Args}). 
add_task(Args) ->
	add_task(task, Args). 



task_result(#task{done=false}) ->
	{wait};
task_result(#task{done=true, status=error, result=Res}) ->
	{error, Res};
task_result(#task{done=true, status=done, result=Res, date=D}) ->
	{ok, Res, lifetime, date_diff(D)};
task_result(#task{done=true, status=_, result=Res}) ->
	{error, uncnown_status, Res}.


get_task(UUID) ->
	case task_machine_db:get_task(UUID) of 
		{empty} ->
			{error, task_not_found};
		{error, Error} ->
			{error, Error};
		{ok, R=#task{}} ->
			task_result(R)
	end.
