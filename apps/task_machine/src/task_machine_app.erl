-module(task_machine_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================


start_cowboy() ->
	application:start(cowboy),
	Dispatch = cowboy_router:compile([
		{'_', [
			{"/tm/[...]", task_machine_handlers, []},
			%%{"/tm/[:command]/", task_machine_handlers, []},
			{'_', task_machine_not_found_handlers, []}
		]}
	]),
	{ok, Port} = application:get_env(task_machine, port),

	{ok, _} = cowboy:start_http(http, 100, [{port, Port}], [
		{env, [{dispatch, Dispatch}]}
	]).


start(_StartType, _StartArgs) ->
	application:start(lager),
	lager:log(info, self(), "Start Task machine application..."),
	case application:get_env(task_machine, need_install_db, false) of
		true -> mnesia:create_schema([node()|nodes()]); %% Тупо не знаю почему приходит true хотя должно {ok, true}
		{ok, true} -> mnesia:create_schema([node()|nodes()]);
		_ -> ok
	end,
	mnesia:start(),
	mnesia:info(),

	case application:get_env(task_machine, need_install_db, false) of
		true -> task_machine_db:create_tables([node()|nodes()]);
		{ok, true} -> task_machine_db:create_tables([node()|nodes()]);
		_ -> mnesia:wait_for_tables([task], 6000) 
	end,

	application:start(crypto),
	application:start(ranch),
	start_cowboy(),
 	task_machine_sup:start_link().


stop(_State) ->
    ok.
