APPNAME = task_machine

all: deps compile

deps:
	rebar get-deps

compile: clean
	rebar compile

clean: devclean
	rm -rf tmp
	rebar clean

run:
	rebar compile
	(mkdir -p tmp && mkdir -p tmp/mnesia)
	erl -config config/task-machine -pa apps/*/ebin deps/*/ebin -eval '[ application:start(A) || A <- [crypto, $(APPNAME)] ]'

install:
	rebar compile
	erl -config config/task-machine -pa apps/*/ebin deps/*/ebin -eval 'task_machine_db:install([node()|nodes()])'


rel: compile
	(cd rel && rebar generate)

devrel: dev1 dev2


dev1 dev2:
	mkdir -p dev
	(cd rel && rebar generate target_dir=../dev/$@ overlay_vars=../vars/$@_vars.config)


dev1console:
	(sh ./dev/dev1/bin/task_machine console)

dev2console:
	(sh ./dev/dev2/bin/task_machine console)


devclean:
	rm -rf dev
